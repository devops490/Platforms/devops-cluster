# MVP

- Setup Minikube
- Lens Prometheus Config
- Add certificates for Https
- Keycloak
- Ambassador

# v1 - CI/CD

- Harbor
- ArgoCD
- Tekton

# v2 - Development addons

- Vault
- Consul
- Atlantis

# v3 - Inculcating DevSecOps

- SonarQube

# v4 - Setting up monitoring and Observability

- Remove Lens Prometheus Config
- Prometheus
- Thanos
- Grafana
- Setup Lens to use Prometheus

# v5 - Integrations

- Discord Integrations
    - ArgoCD
    - Sonarqube
- Keycloak integrations

# v6 - Public Cloud Setup

- AWS
- Azure
- GCP

# v7 - Private cloud Setup

- Virtualbox
- Openshift
- Openstack

# v8 - Setting service URLs

- Compare nip.io vs xip.io for local
- Access services using URLs: "service.domain.<n/x>ip.io"

# v9 - Development

- Backstage
- Yunion Cloud
- Reliably
- Keptn
- Budibase

# v10 - Devops as a Service

- Refactor to microservices

# v11 - AWS Deployment

- Architecture decesions
- Deployment

# v12 - GCP deployment

- Training
- Architecture Decesions
- Deployment

# v13 - Azure deployment

- Training
- Architecture Decesions
- Deployment

# v14 - DigitalOcean

- Training
- Architecture Decesions
- Deployment

# V15 - MultiCloud Deployment

- Training
- Architecture Decesions
- Deployment

# v16 - Openshift deployment

- Architecture Decesions
- Deployment

# v17 - Openstack deployment

- Training
- Architecture Decesions
- Deployment

# V18 - Hybrid Cloud Deployment

- Training
- Architecture Decesions
- Deployment

# v19 - Cost Optimizations for Multicloud models

- Architecture Decesions
- Deployment

# v20 - Cost Optimizations for Hybridcloud models

- Architecture Decesions
- Deployment

# Maintenance