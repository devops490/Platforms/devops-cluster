#!/bin/bash

env=$1
K8S_VERSION="v1.21.1"
NODES=3

install_minikube(){
    curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
    chmod +x minikube
    sudo mkdir -p /usr/local/bin/
    sudo install minikube /usr/local/bin/
}

check_dependencies(){
    # Kubectl
    kubectl version --client &> /dev/null
    if [ $? -ne 0 ]
    then
        echo "Kubectl not found"
        exit 1
    fi

    # Docker
    docker version &> /dev/null
    if [ $? -ne 0 ]
    then
        echo "Docker not found"
        exit 1
    fi

    # Minikube
    minikube version &> /dev/null
    if [ $? -ne 0 ]
    then
        echo "Minikube not found.. Installing"
        install_minikube
    fi
}

start_cluster(){
    environment=$1
    k8s_version=$2
    nodes=$3

    cluster_status=`minikube status -p $environment | grep host | awk '{print $2}'`

    # If cluster does not exist or is stopped
    if [[ -z $cluster_status || $cluster_status == "Stopped" ]]
    then
        minikube start \
            -p $environment \
            --addons=pod-security-policy \
            --container-runtime=containerd \
            --extra-config=apiserver.enable-admission-plugins=PodSecurityPolicy \
            --nodes $nodes \
            --driver=docker \
            --kubernetes-version=$k8s_version
    fi

    # If cluster is running
    if [[ $cluster_status == "Running" ]]
    then
        minikube profile $environment
    fi  
}

set_vars(){
    export MINIKUBE_IP=`minikube ip -p $1`
    export K8S_HOSTNAME="$MINIKUBE_IP.nip.io"
}

install_charts(){
    sleep 30

    # Keycloak
    kubectl apply -f config/keycloak/main.yaml
    cat config/keycloak/ingress.yaml | sed "s/KEYCLOAK_HOST/$K8S_HOSTNAME/" | kubectl create -f -

    # Ambassador
    kubectl apply -f config/Ambassador/aes-crds.yaml && \
    kubectl wait --for condition=established --timeout=180s crd -lproduct=aes && \
    kubectl apply -f config/Ambassador/aes.yaml && \
    kubectl -n ambassador wait --for condition=available --timeout=180s deploy -lproduct=aes

    # Harbor
    helm install harbor bitnami/harbor

    # ArgoCD
    kubectl create namespace argocd
    kubectl apply -n argocd -f config/Argocd/install.yaml


    sleep 30
}

show_banner(){
    minikube service list -p $1
    echo "Harbour Creds"
    echo "==============================="
    echo "Username: admin"
    HARBOR_PASSWORD=`kubectl get secret --namespace default harbor-core-envvars -o jsonpath="{.data.HARBOR_ADMIN_PASSWORD}" | base64 --decode`
    echo "Password: $HARBOR_PASSWORD"

}

delete_cluster(){
    minikube delete -p $1
}

main(){
    if [[ $@ =~ (^|[[:space:]])"-d"($|[[:space:]]) ]] && true || false 
    then
        delete_cluster $env
    else
        check_dependencies
        start_cluster $env $K8S_VERSION $NODES
        set_vars $env
        install_charts
        show_banner $env
    fi
}

main $@